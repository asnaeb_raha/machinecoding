package TicTacToe;

import TicTacToe.Models.Player;
import TicTacToe.Services.GameInitializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class index {

    public static void main(String[] args) throws IOException {
        GameInitializer gameInitializer = new GameInitializer();
        gameInitializer.initializeGameBoard(3);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please enter the number of participants: ");
        int playerCount = Integer.parseInt(bufferedReader.readLine());

        for(int i = 0;i<playerCount;i++){
            System.out.println("Please enter the name of player " + (i + 1) + ": ");
            String playerName = bufferedReader.readLine();
            System.out.println("Please enter the piece choice: ");
            char piece = bufferedReader.readLine().charAt(0);
            Player player = new Player(playerName, piece, i + 1);
            gameInitializer.initializePlayer(player);
        }
        ArrayList<Player> playerList = gameInitializer.getPlayerList();
        System.out.println("Player "+ playerList.get(0).getPlayerName() + " has chosen, "+playerList.get(0).getPieceChoice());
        System.out.println("Player "+ playerList.get(1).getPlayerName() + " has chosen, "+playerList.get(1).getPieceChoice());

        gameInitializer.displayGameBoard();
        System.out.println("Let the games begin! --->");
        System.out.println("Press 's' to start the game");
        System.out.println("Type 'exit' to end game");
        String feedback = bufferedReader.readLine();
        if(feedback.equals("exit")) {
            System.exit(0);
        }else if(!feedback.equals("s")){
            System.out.println("Please enter a proper choice");
        }
        //for player 1
        Player winner = null;
        while(true){
            Player currentPlayer = gameInitializer.getCurrentPlayer();
            System.out.println("Player- "+ currentPlayer.getPlayerName()+"'s turn now");
            System.out.println("Please enter cell, Enter x coordinate- ");
            int xCoordinate = Integer.parseInt(bufferedReader.readLine());
            System.out.println("Please enter cell, Enter y coordinate- ");
            int yCoordinate = Integer.parseInt(bufferedReader.readLine());
            boolean moveResult = gameInitializer.makeMove(xCoordinate, yCoordinate, currentPlayer.getPieceChoice());
            if(!moveResult) {
                System.out.println("There is already a piece in the position, Please try again: ");
                System.exit(1);
            }
            gameInitializer.displayGameBoard();
            winner = gameInitializer.determineVictory(xCoordinate, yCoordinate, currentPlayer);
            if(winner != null || gameInitializer.isGridFull()){
                break;
            }
        }
        if(winner == null){
            System.out.println("THIS MATCH IS A TIE!");
            return;
        }
        System.out.println("Congratulations! Player " + winner.getPlayerName() +", You have wont the match");
    }

}
