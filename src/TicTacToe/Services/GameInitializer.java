package TicTacToe.Services;

import TicTacToe.Models.GameBoard;
import TicTacToe.Models.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class GameInitializer {
    private GameBoard gameBoard;
    private ArrayList<Player> playerList;
    private Queue<Player> playQueue;

    private Integer gridCount = 0;

    public GameInitializer() {
        this.playerList = new ArrayList<>();
        this.playQueue = new LinkedList<>();
    }

    public void initializeGameBoard(int boardSize){
         gameBoard = new GameBoard(boardSize);
        char[][] board = gameBoard.getBoard();

        //initialize board with '_'
        for(char[] boardRow : board){
            Arrays.fill(boardRow, '_');
        }
    }

    public Player getCurrentPlayer(){
        Player currentPlayer = playQueue.peek();
        playQueue.remove();
        playQueue.add(currentPlayer);
        return currentPlayer;
    }

    public void initializePlayer(Player player){
        playerList.add(player);
        playQueue.add(player);
    }

    public boolean displayGameBoard(){
        char[][] board = gameBoard.getBoard();
        if(board.length == 0) return false;

        for(char[] boardRow : board){
            System.out.println(boardRow);
        }
        return true;
    }

    public ArrayList<Player> getPlayerList(){
        if(playerList.size() == 0) return null;
        return playerList;
    }

    public boolean makeMove(int xCoordinate, int yCoordinate, char piece){
        char[][] board = gameBoard.getBoard();
        if(board[xCoordinate][yCoordinate] == '_'){
            board[xCoordinate][yCoordinate] = piece;
            gridCount++;
            return true;
        }
        return false;
    }

    public boolean isGridFull(){
        int boardSize = gameBoard.getBoardSize();
        return gridCount == boardSize;
    }

    public Player determineVictory(int xCoordinate, int yCoordinate, Player currentPlayer){
        //1 - player 1 has won
        //2 - player 2 has won
        // INT_MAX - tie
        //-1 - game ongoing

        char piece = currentPlayer.getPieceChoice();
        char[][] board = gameBoard.getBoard();
        int boardSize = board.length;

        //check all rows
        for (char[] chars : board) {
            int count = 0;
            for (int j = 0; j < boardSize; j++) {
                if (chars[j] == piece) {
                    count++;
                }
            }
            if (count == boardSize) return currentPlayer;
        }

        //check all columns
        for(int j = 0;j<boardSize;j++){
            int count = 0;
            for (char[] chars : board) {
                if (chars[j] == piece) {
                    count++;
                }
            }

            if(count == boardSize) return currentPlayer;
        }

        //check top left to bottom right diagonal
        for(int i = 0;i<boardSize;i++){
            int count = 0;
            if(board[i][i] == piece){
                count++;
            }
            if(count == boardSize) return currentPlayer;
        }

        //check bottom left to top right diagonal

        for(int i = 0, j = boardSize - 1;j>=0;i++, j--){
            int count = 0;
            if(board[i][j] == piece){
                count++;
            }
            if(count == boardSize) return currentPlayer;
        }
        return null;
    }
}
