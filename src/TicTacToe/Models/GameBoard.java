package TicTacToe.Models;

public class GameBoard {
    private char[][] board;
    private Integer boardSize;

    public GameBoard(Integer boardSize) {
        this.boardSize = boardSize;
        board = new char[boardSize][boardSize];
    }

    public char[][] getBoard() {
        return board;
    }

    public void setBoard(char[][] board) {
        this.board = board;
    }

    public Integer getBoardSize() {
        return boardSize;
    }

    public void setBoardSize(Integer boardSize) {
        this.boardSize = boardSize;
    }
}
