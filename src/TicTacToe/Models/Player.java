package TicTacToe.Models;

public class Player {
    private Integer playerID;
    private String playerName;
    private char pieceChoice; //x o
    private Integer score;

    public Player(String playerName, char pieceChoice, Integer playerID) {
        this.playerID = playerID;
        this.playerName = playerName;
        this.pieceChoice = pieceChoice;
    }

    public Integer getPlayerID() {
        return playerID;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public char getPieceChoice() {
        return pieceChoice;
    }

    public void setPieceChoice(char pieceChoice) {
        this.pieceChoice = pieceChoice;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
